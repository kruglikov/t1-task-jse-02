package ru.t1.kruglikov.tm;

import static ru.t1.kruglikov.tm.constant.TerminalConst.*;

public class Application {
    private static void displayError() {
        System.err.println("[ERROR]");
        System.err.println("Argument is not supported.");
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show info about developer. \n", CMD_ABOUT);
        System.out.printf("%s - Show program version. \n", CMD_VERSION);
        System.out.printf("%s - Show list arguments. \n", CMD_HELP);
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Kruglikov Andrey");
        System.out.println("akruglikov@t1-consulting.ru");
        System.exit(0);
    }

    private static void run(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_HELP:
                displayHelp();
                break;
        }
    }

    public static void main(final String[] args) {
        if (args == null || args.length == 0) {
            displayError();
            return;
        }
        run(args[0]);
    }
}
